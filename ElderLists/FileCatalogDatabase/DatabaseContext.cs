﻿using FileCatalogDatabase.Models;
using FileCatalogDatabase.Models.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System;
using System.Threading.Tasks;

namespace FileCatalogDatabase
{
    public class DatabaseContext : IdentityDbContext<User,Role,int,UserLogin,UserRole,UserClaim>
    {
        // Identity
        // public virtual DbSet<User> Users { get; set; }   // Перегрузка IdentityDbContext
        // public virtual DbSet<Role> Roles { get; set; }   // Перегрузка IdentityDbContext
        public virtual DbSet<UserClaim> UserClaims { get; set; }
        public virtual DbSet<UserLogin> UserLogins { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }

        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<File> Files { get; set; }
        public virtual DbSet<Edition> Editions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Создание правильных имен для таблиц Identity
            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<Role>().ToTable("Roles");
            modelBuilder.Entity<UserRole>().ToTable("UserRoles");
            modelBuilder.Entity<UserLogin>().ToTable("UserLogins");
            modelBuilder.Entity<UserClaim>().ToTable("UserClaims");

            // Убираем каскадное удаление
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public DatabaseContext() : base("DefaultConnection")
        {
        }

        public DatabaseContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
        }

        public static DatabaseContext Create()
        {
            return new DatabaseContext();
        }
    }
}
