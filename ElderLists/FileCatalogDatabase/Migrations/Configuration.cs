namespace FileCatalogDatabase.Migrations
{
    using FileCatalogDatabase.Initializers;
    using FileCatalogDatabase.Models.Identity;
    using Microsoft.AspNet.Identity;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<FileCatalogDatabase.DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        private List<IInitializer> GetInitializersList()
        {
            return new List<IInitializer>
            {
                new Initializers.Ru.RoleInit(),
            };
        }

        protected override void Seed(FileCatalogDatabase.DatabaseContext context)
        {
            var list = GetInitializersList();
            foreach (var init in list)
                init.Seed(context);
            context.SaveChanges();
        }
    }
}
