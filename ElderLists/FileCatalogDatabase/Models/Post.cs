﻿using FileCatalogDatabase.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCatalogDatabase.Models
{
    public class Post
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(128)]
        [Index(IsUnique = true)]
        public string Header { get; set; }

        [Required]
        [StringLength(2048)]
        public string Text { get; set; }

        [Required]
        public bool IsHidden { get; set; }

        [Required]
        public bool IsImportant { get; set; }

        [Required]
        public DateTime CreationDate { get; set; }

        [Required]
        [ForeignKey("Author")]
        public int AuthorId { get; set; }
        public virtual User Author { get; set; }

        [ForeignKey("PostId")]
        public virtual ICollection<File> Files { get; set; }

        [ForeignKey("PostId")]
        public virtual ICollection<Edition> Editions { get; set; }
    }
}
