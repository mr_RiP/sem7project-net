﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCatalogDatabase.Initializers
{
    interface IInitializer
    {
        void Seed(DatabaseContext context);
    }
}
