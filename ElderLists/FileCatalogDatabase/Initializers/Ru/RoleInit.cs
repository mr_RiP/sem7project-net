﻿using FileCatalogDatabase.Models.Identity;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCatalogDatabase.Initializers.Ru
{
    class RoleInit : IInitializer
    {
        public void Seed(DatabaseContext context)
        {
            var admin = new Role    { Name = "Admin",           ViewName = "Администратор"  };
            var moder = new Role    { Name = "Moderator",       ViewName = "Модератор"      };
            var user =  new Role    { Name = "User",            ViewName = "Пользователь"   };
            var banned = new Role   { Name = "Banned",          ViewName = "Забанен"        };

            context.Roles.AddOrUpdate(o => o.Name, admin, moder, user, banned);
            context.SaveChanges();
        }
    }
}
