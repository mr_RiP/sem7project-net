﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FileCatalog.Startup))]
namespace FileCatalog
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
