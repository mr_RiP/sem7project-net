﻿using FileCatalog.Extensions;
using FileCatalog.Models;
using PagedList;
using PagedList.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FileCatalog.Repositories
{
    public class UsersRepository : DatabaseRepositoryBase
    {
        public async Task<IPagedList<UserListItemViewModel>> GetUsersListAsync(string roleName, PagedListOptions options)
        {
            int roleId = await Context.Roles.Where(r => r.Name == roleName).Select(r => r.Id).FirstOrDefaultAsync();
            var userIds = Context.UserRoles.Where(ur => ur.RoleId == roleId).Select(ur => ur.UserId);
            var users = Context.Users.Join(userIds, u => u.Id, i => i, (u, i) => u);

            if (options.Search != null)
                users = users.Where(u => u.UserName.Contains(options.Search));

            return await users
                .OrderByDescending(u => u.LastAction)
                .Include(u => u.Posts)
                .Include(u => u.Editions)
                .Select(u => new UserListItemViewModel
                {
                    Id = u.Id,
                    UserName = u.UserName,
                    LastAction = u.LastAction,
                    Posts = u.Posts.Count,
                    Editions = u.Editions.Count,
                })
                .ToPagedListAsync(options.Page, options.PageSize);
        }

        public async Task<UserDetailsViewModel> GetUserDetailsAsync(int id)
        {
            UserRolesViewModel roles = await GetUserRolesAsync(id);
            UserStatsViewModel stats = await GetUserStatsAsync(id);

            var user = await Context.Users.FirstOrDefaultAsync(u => u.Id == id);
            var contacts = new UserContactsViewModel(user);

            return new UserDetailsViewModel
            {
                Id = user.Id,
                UserName = user.UserName,
                Email = user.Email,
                IsEmailHidden = user.IsEmailHidden,
                About = user.About,
                RegisterDate = user.RegisterDate,
                LastAction = user.LastAction,

                Contacts = contacts,
                Roles = roles,
                Stats = stats,
            };
        }

        private async Task<UserStatsViewModel> GetUserStatsAsync(int id)
        {
            var postsIds = Context.Posts.Where(p => p.AuthorId == id).Select(p => p.Id);
            int posts = await postsIds.CountAsync();
            int files = await Context.Files.Join(postsIds, f => f.PostId, i => i, (f, i) => f).CountAsync();
            int editions = await Context.Editions.CountAsync(e => e.AuthorId == id);

            return new UserStatsViewModel
            {
                Posts = posts,
                Files = files,
                Editions = editions,
            };
        }

        private async Task<UserRolesViewModel> GetUserRolesAsync(int id)
        {
            var roleIds = Context.UserRoles.Where(ur => ur.UserId == id).Select(ur => ur.RoleId);
            var roles = await Context.Roles.Join(roleIds, r => r.Id, i => i, (r, i) => new { Name = r.Name, ViewName = r.ViewName }).ToListAsync();

            return new UserRolesViewModel
            {
                Roles = roles.Select(r => r.ViewName).ToList(),
                IsAdmin = roles.Any(r => r.Name == "Admin"),
                IsModerator = roles.Any(r => r.Name == "Moderator"),
                IsUser = roles.Any(r => r.Name == "User"),
                IsBanned = roles.Any(r => r.Name == "Banned"),
            };
        }
    }
}