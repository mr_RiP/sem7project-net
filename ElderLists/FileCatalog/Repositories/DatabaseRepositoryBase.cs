﻿using FileCatalogDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileCatalog.Repositories
{
    public class DatabaseRepositoryBase : IDisposable
    {
        public DatabaseRepositoryBase()
        {
            Context = new DatabaseContext();
        }

        public DatabaseRepositoryBase(DatabaseContext db)
        {
            Context = db ?? throw new ArgumentNullException();
        }

        public DatabaseContext Context { get; private set; }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}