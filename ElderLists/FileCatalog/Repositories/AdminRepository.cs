﻿using FileCatalog.Extensions;
using FileCatalogDatabase.Models.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FileCatalog.Repositories
{
    public class AdminRepository : DatabaseRepositoryBase
    {
        public async Task<bool> UserHaveAccessAsync(int userId)
        {
            return await Context.IsUserNotBannedAsync(userId) && await Context.IsUserInRoleAsync(userId, "Admin");
        }

        public async Task ApplyRoleAsync(int userId, int adminId, string roleName)
        {
            var role = await Context.Roles.FirstOrDefaultAsync(r => r.Name == roleName);
            var user = await Context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (role != null && user != null && !(await Context.UserRoles.AnyAsync(ur => ur.UserId == user.Id && ur.RoleId == role.Id)))
            {
                Context.UserRoles.Add(new UserRole { UserId = user.Id, RoleId = role.Id });
                await Context.SaveChangesAsync();
            }    
            
            await Context.UpdateUserLastActionAsync(adminId, DateTime.Now);
        }

        public async Task DenyRoleAsync(int userId, int adminId, string roleName)
        {
            var role = await Context.Roles.FirstOrDefaultAsync(r => r.Name == roleName);
            var user = await Context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (role != null && user != null)
            {
                var userRole = await Context.UserRoles.FirstOrDefaultAsync(ur => ur.UserId == user.Id && ur.RoleId == role.Id);
                if (userRole != null)
                {
                    Context.UserRoles.Remove(userRole);
                    await Context.SaveChangesAsync();
                }
            }

            await Context.UpdateUserLastActionAsync(adminId, DateTime.Now);
        }
    }
}