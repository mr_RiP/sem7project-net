﻿using FileCatalogDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using FileCatalog.Models;
using System.Data.Entity;
using FileCatalog.Extensions;
using FileCatalogDatabase.Models;
using PagedList;
using PagedList.EntityFramework;

namespace FileCatalog.Repositories
{
    public class PostsRepository : DatabaseRepositoryBase
    {
        public PostsRepository() : base() { }

        public PostsRepository(DatabaseContext db) : base(db) { }

        public async Task<PostsHeadersListsViewModel> CreateHeadersListModelAsync(int? currentUserId, int? targetUser, PagedListOptions options)
        {
            IQueryable<Post> posts = Context.Posts;
            posts = ApplyTargetUser(posts, targetUser);
            posts = await ApplyAccessAsync(posts, currentUserId);

            IQueryable<Edition> editions = GetLastEditions();

            var importantList = await GetImportantHeadersListAsync(posts, editions);
            var commonList = await GetCommonHeadersPagedListAsync(posts, editions, options);

            return new PostsHeadersListsViewModel { Important = importantList, Common = commonList };
        }

        private async Task<IPagedList<PostHeaderViewModel>> GetCommonHeadersPagedListAsync(IQueryable<Post> posts, IQueryable<Edition> lastEditions, PagedListOptions options)
        {
            posts = posts.Where(p => !p.IsImportant).Include(p => p.Author);
            if (!String.IsNullOrEmpty(options.Search))
                posts = posts.Where(p => p.Header.Contains(options.Search));

            return await posts
                .Select(p => new { PostData = p, LastEdition = lastEditions.FirstOrDefault(e => e.PostId == p.Id) })
                .Select(s => new PostHeaderViewModel
                {
                    Id = s.PostData.Id,
                    Header = s.PostData.Header,
                    AuthorId = s.PostData.AuthorId,
                    Author = s.PostData.Author.UserName,
                    EditorId = s.LastEdition == null ? (int?)null : s.LastEdition.AuthorId,
                    Editor = s.LastEdition == null ? null : s.LastEdition.Author.UserName,
                    CreationDate = s.PostData.CreationDate,
                    LastEditDate = s.LastEdition == null ? (DateTime?)null : s.LastEdition.Date,
                    IsHidden = s.PostData.IsHidden
                })
                .OrderByDescending(vm => vm.LastEditDate == null ? vm.CreationDate : vm.LastEditDate)
                .ToPagedListAsync(options.Page, options.PageSize);
        }

        public string HeaderAlreadyInUseMessage()
        {
            return "Такой заголовок уже используется в другой публикации";
        }

        public async Task<bool> IsHeaderAlreadyInUseAsync(string header, int? id = null)
        {
            if (id == null)
                return await Context.Posts.AnyAsync(p => p.Header == header);
            else
                return await Context.Posts.AllAsync(p => p.Header == header && p.Id != id);
        }

        private IQueryable<Edition> GetLastEditions()
        {
            return Context.Editions
                .GroupBy(e => e.PostId)
                .Select(eds => eds.OrderByDescending(e => e.Date).FirstOrDefault());
        }

        public async Task<bool> UserCanCreateAsync(int userId)
        {
            return await Context.IsUserNotBannedAsync(userId) && await Context.IsUserInRoleAsync(userId, "User");
        }

        private async Task<IList<PostHeaderViewModel>> GetImportantHeadersListAsync(IQueryable<Post> posts, IQueryable<Edition> lastEditions)
        {
            return await posts
                .Where(p => p.IsImportant)
                .Include(p => p.Author)
                .Select(p => new { PostData = p, LastEdition = lastEditions.FirstOrDefault(e => e.PostId == p.Id) })
                .Select(s => new PostHeaderViewModel
                {
                    Id = s.PostData.Id,
                    Header = s.PostData.Header,
                    AuthorId = s.PostData.AuthorId,
                    Author = s.PostData.Author.UserName,
                    EditorId = s.LastEdition == null ? (int?)null : s.LastEdition.AuthorId,
                    Editor = s.LastEdition == null ? null : s.LastEdition.Author.UserName,
                    CreationDate = s.PostData.CreationDate,
                    LastEditDate = s.LastEdition == null ? (DateTime?)null : s.LastEdition.Date,
                    IsHidden = s.PostData.IsHidden
                })
                .OrderByDescending(vm => vm.LastEditDate == null ? vm.CreationDate : vm.LastEditDate)
                .ToListAsync();
        }

        private IQueryable<Post> ApplyTargetUser(IQueryable<Post> posts, int? targetUser)
        {
            if (targetUser == null)
                return posts;

            var userPostsIds = posts.Where(p => p.AuthorId == targetUser).Select(p => p.Id);
            var userEditionsPostIds = Context.Editions.Where(e => e.AuthorId == targetUser).Select(e => e.PostId);
            var postIds = userPostsIds.Union(userEditionsPostIds);
            return posts.Join(postIds, p => p.Id, i => i, (p, i) => p);
        }

        private async Task<IQueryable<Post>> ApplyAccessAsync(IQueryable<Post> posts, int? currentUserId)
        {
            return currentUserId != null && await Context.IsUserInRoleAsync((int)currentUserId, "Admin") ? posts : posts.Where(p => !p.IsHidden);
        }

        public async Task<bool> PostExistsAsync(int id)
        {
            return await Context.Posts.Where(p => p.Id == id).AnyAsync();
        }

        public async Task<PostDetailsViewModel> CreateDetailsModelAsync(int postId)
        {
            var post = await Context.Posts.FirstOrDefaultAsync(p => p.Id == postId);
            var files = await GetFilesListAsync(postId);
            var editions = await GetEditionsListAsync(postId);

            return new PostDetailsViewModel
            {
                Id = post.Id,
                Header = post.Header,
                Text = post.Text,
                IsHidden = post.IsHidden,
                IsImportant = post.IsImportant,
                CreationDate = post.CreationDate,
                AuthorId = post.AuthorId,
                Author = post.Author.UserName,
                Files = files,
                Editions = editions,
            };
        }

        private async Task<List<EditionViewModel>> GetEditionsListAsync(int postId)
        {
            return await Context.Editions
                .OrderByDescending(e => e.Date)
                .Where(e => e.PostId == postId)
                .Include(e => e.Author)
                .Select(e => new EditionViewModel
                {
                    Date = e.Date,
                    Note = e.Note,
                    EditorId = e.AuthorId,
                    Editor = e.Author.UserName,
                })
                .ToListAsync();
        }

        public async Task<bool> IsUserCanDownloadAsync(int userId)
        {
            return await Context.IsUserNotBannedAsync(userId) && await Context.IsUserInRoleAsync(userId, "User");
        }

        public async Task<Tuple<byte[], string>> GetFileDataAsync(int userId, int fileId, string path)
        {
            var fileBytes = System.IO.File.ReadAllBytes(System.IO.Path.Combine(path, fileId.ToString()));

            var time = DateTime.Now;

            var file = await Context.Files.Where(f => f.Id == fileId).FirstOrDefaultAsync();
            file.DownloadsCount++;
            file.LastDownload = time;

            var user = await Context.Users.Where(u => u.Id == userId).FirstOrDefaultAsync();
            user.LastAction = time;

            await Context.SaveChangesAsync();
            return new Tuple<byte[], string>(fileBytes, file.Name);
        }

        public async Task<PostHeaderBaseViewModel> GetPostHeaderBaseModelAsync(int id)
        {
            return await Context.Posts.Where(p => p.Id == id).Select(p => new PostHeaderBaseViewModel { Id = p.Id, Header = p.Header }).FirstOrDefaultAsync();
        }

        public async Task DeletePostAsync(int userId, int postId)
        {
            Context.Files.RemoveRange(Context.Files.Where(f => f.PostId == postId));
            Context.Editions.RemoveRange(Context.Editions.Where(e => e.PostId == postId));
            Context.Posts.Remove(await Context.Posts.FirstOrDefaultAsync(p => p.Id == postId));
            await Context.SaveChangesAsync();

            await Context.UpdateUserLastActionAsync(userId, DateTime.Now);
        }

        public async Task<bool> UserCanDeleteAsync(int userId, int id)
        {
            if (await Context.IsUserNotBannedAsync(userId))
            {
                int authorId = await Context.Posts.Where(p => p.Id == id).Select(p => p.AuthorId).FirstOrDefaultAsync();
                return (authorId == userId && await Context.IsUserInRoleAsync(userId, "User")) || await Context.IsUserInRoleAsync(userId, "Admin");
            }
            else
                return false;
        }

        public async Task<bool> FileExistsAsync(int id, string path)
        {
            return await Context.Files.AnyAsync(f => f.Id == id) && 
                System.IO.File.Exists(System.IO.Path.Combine(path, id.ToString()));
        }

        private async Task<List<FileViewModel>> GetFilesListAsync(int postId)
        {
            return await Context.Files
                .Where(f => f.PostId == postId)
                .Select(f => new FileViewModel
                {
                    Id = f.Id,
                    Name = f.Name,
                    DownloadsCount = f.DownloadsCount,
                    LastDownloadDate = f.LastDownload,
                    UploadDate = f.UploadDate,
                })
                .ToListAsync();
        }

        public async Task AddNewPostAsync(int userId, string path, NewPostViewModel model, IEnumerable<HttpPostedFileBase> files)
        {
            var time = DateTime.Now;
            int postId = await AddPostAsync(userId, time, model);
            await AddFilesAsync(postId, path, time, files);
            await Context.UpdateUserLastActionAsync(userId, time);
        }

        private async Task<int> AddPostAsync(int userId, DateTime time, NewPostViewModel model)
        {
            var post = new Post
            {
                AuthorId = userId,
                Text = model.Text,
                Header = model.Header,
                IsHidden = model.IsHidden,
                IsImportant = model.IsImportant,
                CreationDate = time,
            };

            Context.Posts.Add(post);
            await Context.SaveChangesAsync();

            return post.Id;
        }

        private async Task AddFilesAsync(int postId, string path, DateTime time, IEnumerable<HttpPostedFileBase> files)
        {
            if (files == null)
                return;
 
            files = files.Where(f => f != null && f.ContentLength > 0);
            if (files.Any())
            {
                var dbList = files.Select(f => new File
                {
                    Name = f.FileName,
                    PostId = postId,
                    UploadDate = time,
                    DownloadsCount = 0,
                    LastDownload = null,
                
                })
                .ToList();

                Context.Files.AddRange(dbList);
                await Context.SaveChangesAsync();

                var fsList = dbList.Join(files, dbFile => dbFile.Name, httpFile => httpFile.FileName, (dbFile, httpFile) => new { Id = dbFile.Id, HttpFile = httpFile }).ToList();
                fsList.ForEach(f => f.HttpFile.SaveAs(System.IO.Path.Combine(path, f.Id.ToString())));
            }
        }

        public async Task<bool> UserCanEditAsync(int userId, int postId)
        {
            var post = await Context.Posts.Where(p => p.Id == postId).FirstOrDefaultAsync();
            if (post != null && await Context.IsUserNotBannedAsync(userId))
                return (post.AuthorId == userId && await Context.IsUserInRoleAsync(userId, "User")) || await Context.IsUserInRoleAsync(userId, "Moderator");
            else
                return false;
        }

        public async Task<EditPostViewModel> CreateEditModelAsync(int id)
        {
            var post = await Context.Posts.FirstOrDefaultAsync(p => p.Id == id);
            var files = await GetFileEditViewModels(id);

            return new EditPostViewModel
            {
                Id = post.Id,
                Header = post.Header,
                Text = post.Text,
                IsHidden = post.IsHidden,
                IsImportant = post.IsImportant,
                CurrentFiles = files,
            };
        }

        public async Task<List<FileEditViewModel>> GetFileEditViewModels(int postId)
        {
            return await Context.Files.Where(f => f.PostId == postId)
                .Select(f => new FileEditViewModel
                {
                    Id = f.Id,
                    Name = f.Name,
                    DownloadsCount = f.DownloadsCount,
                    LastDownloadDate = f.LastDownload,
                    UploadDate = f.UploadDate,
                    Delete = false,
                })
                .ToListAsync();
        }

        public async Task EditPostAsync(int userId, string path, EditPostViewModel model, IEnumerable<HttpPostedFileBase> files)
        {
            var time = DateTime.Now;

            await EditPostDataAsync(userId, time, model);
            await DeleteFilesAsync(path, model);
            await AddFilesAsync(model.Id, path, time, files);
            await Context.UpdateUserLastActionAsync(userId, time);
        }

        private async Task DeleteFilesAsync(string path, EditPostViewModel model)
        {
            var idList = model.CurrentFiles.Where(m => m.Delete).Select(m => m.Id).ToList();

            idList.ForEach(id => System.IO.File.Delete(System.IO.Path.Combine(path, id.ToString())));
            Context.Files.RemoveRange(Context.Files.Where(f => idList.Contains(f.Id)));

            await Context.SaveChangesAsync();
        }

        private async Task EditPostDataAsync(int userId, DateTime time, EditPostViewModel model)
        {
            var post = await Context.Posts.FirstOrDefaultAsync(p => p.Id == model.Id);

            post.Header = model.Header;
            post.Text = model.Text;
            post.IsHidden = model.IsHidden;
            post.IsImportant = model.IsImportant;

            var edition = new Edition
            {
                AuthorId = userId,
                Note = model.Note,
                Date = time,
                PostId = post.Id,
            };
     
            Context.Editions.Add(edition);
            await Context.SaveChangesAsync();
        }
    }
}