﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FileCatalogDatabase;
using FileCatalogDatabase.Models.Identity;
using FileCatalog.Models;
using Microsoft.AspNet.Identity;
using FileCatalog.Extensions;
using FileCatalog.Repositories;

namespace FileCatalog.Controllers
{
    public class UsersController : Controller
    {
        private UsersRepository repository = new UsersRepository();

        public async Task<ActionResult> Index(string search, int? page)
        {
            if (User.Identity.IsAuthenticated && await repository.Context.IsUserBannedAsync(User.Identity.GetUserId<int>()))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            ViewBag.Search = search;
            return View(await repository.GetUsersListAsync("User", new PagedListOptions(search, page)));
        }
        
        public async Task<ActionResult> Moderators(string search, int? page)
        {
            if (User.Identity.IsAuthenticated && await repository.Context.IsUserBannedAsync(User.Identity.GetUserId<int>()))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            ViewBag.Search = search;
            return View(await repository.GetUsersListAsync("Moderator", new PagedListOptions(search, page)));
        }

        public async Task<ActionResult> Banned(string search, int? page)
        {
            if (User.Identity.IsAuthenticated && await repository.Context.IsUserBannedAsync(User.Identity.GetUserId<int>()))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            ViewBag.Search = search;
            return View(await repository.GetUsersListAsync("Banned", new PagedListOptions(search, page)));
        }

        [Authorize]
        public ActionResult My()
        {
            return RedirectToAction("Details", new { id = User.Identity.GetUserId<int>() });
        }

        public async Task<ActionResult> Details(int id)
        {
            if (User.Identity.IsAuthenticated && await repository.Context.IsUserBannedAsync(User.Identity.GetUserId<int>()))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            return View(await repository.GetUserDetailsAsync(id));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
