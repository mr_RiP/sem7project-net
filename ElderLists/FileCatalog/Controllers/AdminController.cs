﻿using FileCatalog.Repositories;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FileCatalog.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        AdminRepository repository = new AdminRepository();

        public async Task<ActionResult> ApplyUserRole(int id)
        {
            int userId = User.Identity.GetUserId<int>();
            if (await repository.UserHaveAccessAsync(userId))
            {
                await repository.ApplyRoleAsync(id, userId, "User");
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        public async Task<ActionResult> DenyUserRole(int id)
        {
            int userId = User.Identity.GetUserId<int>();
            if (await repository.UserHaveAccessAsync(userId))
            {
                await repository.DenyRoleAsync(id, userId, "User");
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        public async Task<ActionResult> ApplyModeratorRole(int id)
        {
            int userId = User.Identity.GetUserId<int>();
            if (await repository.UserHaveAccessAsync(userId))
            {
                await repository.ApplyRoleAsync(id, userId, "Moderator");
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        public async Task<ActionResult> DenyModeratorRole(int id)
        {
            int userId = User.Identity.GetUserId<int>();
            if (await repository.UserHaveAccessAsync(userId))
            {
                await repository.DenyRoleAsync(id, userId, "Moderator");
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        public async Task<ActionResult> BanUser(int id)
        {
            int userId = User.Identity.GetUserId<int>();
            if (await repository.UserHaveAccessAsync(userId))
            {
                await repository.ApplyRoleAsync(id, userId, "Banned");
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        public async Task<ActionResult> UnbanUser(int id)
        {
            int userId = User.Identity.GetUserId<int>();
            if (await repository.UserHaveAccessAsync(userId))
            {
                await repository.DenyRoleAsync(id, userId, "Banned");
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }
    }
}