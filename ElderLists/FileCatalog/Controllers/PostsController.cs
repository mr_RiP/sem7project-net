﻿using FileCatalog.Extensions;
using FileCatalog.Models;
using FileCatalog.Repositories;
using FileCatalogDatabase;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FileCatalog.Controllers
{
    public class PostsController : Controller
    {
        private PostsRepository repository = new PostsRepository();

        public async Task<ActionResult> Index(string search, int? page)
        {
            int? userId = User.Identity.IsAuthenticated ? (int?)User.Identity.GetUserId<int>() : null;
            var options = new PagedListOptions(search, page);
            if (userId == null || await repository.Context.IsUserNotBannedAsync((int)userId))
            {
                ViewBag.Search = search;
                return View(await repository.CreateHeadersListModelAsync(userId, null, options));
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        public async Task<ActionResult> Details(int id)
        {
            if (! await repository.PostExistsAsync(id))
                return new HttpNotFoundResult();

            if (!User.Identity.IsAuthenticated || await repository.Context.IsUserNotBannedAsync(User.Identity.GetUserId<int>()))
                return View(await repository.CreateDetailsModelAsync(id));
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        [Authorize]
        public async Task<ActionResult> New()
        {
            if (await repository.UserCanCreateAsync(User.Identity.GetUserId<int>()))
                return View();
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New([Bind(Include = "Header,Text,IsHidden,IsImportant")] NewPostViewModel model, IEnumerable<HttpPostedFileBase> files)
        {
            int userId = User.Identity.GetUserId<int>();
            if (await repository.UserCanCreateAsync(userId))
            {
                if (await repository.IsHeaderAlreadyInUseAsync(model.Header))
                    ModelState.AddModelError("", repository.HeaderAlreadyInUseMessage());

                if (ModelState.IsValid)
                {
                    if (model.IsImportant && !(await repository.Context.IsUserInRoleAsync(userId, "Moderator")))
                        return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

                    var path = Server.MapPath("~/UploadedFiles/");
                    await repository.AddNewPostAsync(userId, path, model, files);
                    return RedirectToAction("Index");
                }
                else
                    return View(model);
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        [Authorize]
        public async Task<ActionResult> Edit(int id)
        {
            if (await repository.PostExistsAsync(id))
            {
                if (await repository.UserCanEditAsync(User.Identity.GetUserId<int>(), id))
                    return View(await repository.CreateEditModelAsync(id));
                else
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            else
                return new HttpNotFoundResult();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Header,Text,IsHidden,IsImportant,Note")] EditPostViewModel model, IEnumerable<FileEditViewModel> currentFiles, IEnumerable<HttpPostedFileBase> newFiles)
        {
            int userId = User.Identity.GetUserId<int>();
            if (await repository.UserCanEditAsync(userId, model.Id))
            {
                model.CurrentFiles = currentFiles ?? new List<FileEditViewModel>(); // HAIL SATAN

                if (await repository.IsHeaderAlreadyInUseAsync(model.Header, model.Id))
                    ModelState.AddModelError("", repository.HeaderAlreadyInUseMessage());

                if (ModelState.IsValid)
                {
                    if (model.IsImportant && !(await repository.Context.IsUserInRoleAsync(userId, "Moderator")))
                        return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

                    var path = Server.MapPath("~/UploadedFiles/");
                    await repository.EditPostAsync(userId, path, model, newFiles);
                    return RedirectToAction("Index");
                }
                else
                    return View(model);
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        [Authorize]
        public async Task<ActionResult> My(string search, int? page)
        {
            int userId = User.Identity.GetUserId<int>();
            if (await repository.Context.IsUserNotBannedAsync(userId))
            {
                ViewBag.Search = search;
                return View(await repository.CreateHeadersListModelAsync(userId, userId, new PagedListOptions(search, page)));
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        public async Task<ActionResult> UserPosts(int id, string search, int? page)
        {
            int userId = User.Identity.GetUserId<int>();
            if (await repository.Context.IsUserNotBannedAsync(userId))
            {
                var userName = await repository.Context.GetUserNameAsync(id);
                if (userName == null)
                    return new HttpNotFoundResult();
                else
                {
                    ViewBag.UserName = userName;
                    ViewBag.UserId = userId;
                    ViewBag.Search = search;
                    return View(await repository.CreateHeadersListModelAsync(userId, id, new PagedListOptions(search, page)));
                }
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        [Authorize]
        public async Task<ActionResult> Download(int id)
        {
            int userId = User.Identity.GetUserId<int>();
            if (await repository.IsUserCanDownloadAsync(userId))
            {
                var path = Server.MapPath("~/UploadedFiles/");
                if (await repository.FileExistsAsync(id, path))
                {
                    Tuple<byte[], string> fileData = await repository.GetFileDataAsync(userId, id, path);
                    return File(fileData.Item1, System.Net.Mime.MediaTypeNames.Application.Octet, fileData.Item2);
                }
                else
                    return new HttpNotFoundResult();
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        [Authorize]
        public async Task<ActionResult> Delete(int id)
        {
            int userId = User.Identity.GetUserId<int>();
            if (await repository.UserCanDeleteAsync(userId, id))
                return View(await repository.GetPostHeaderBaseModelAsync(id));
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(PostHeaderBaseViewModel model)
        {
            int userId = User.Identity.GetUserId<int>();
            if (await repository.UserCanDeleteAsync(userId, model.Id))
            {
                await repository.DeletePostAsync(userId, model.Id);
                return RedirectToAction("Index");
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}