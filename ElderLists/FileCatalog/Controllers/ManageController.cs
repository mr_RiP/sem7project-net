﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using FileCatalog.Models;
using FileCatalogDatabase;
using System.Data.Entity;
using System.Collections.Generic;
using System.Net;
using FileCatalogDatabase.Models.Identity;
using FileCatalog.Extensions;

namespace FileCatalog.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private DatabaseContext db = new DatabaseContext();

        public ManageController()
        {
        }

        public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Manage/Index
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Ваш пароль изменен."
                : message == ManageMessageId.SetPasswordSuccess ? "Пароль задан."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Настроен поставщик двухфакторной проверки подлинности."
                : message == ManageMessageId.Error ? "Произошла ошибка."
                : message == ManageMessageId.AddPhoneSuccess ? "Ваш номер телефона добавлен."
                : message == ManageMessageId.RemovePhoneSuccess ? "Ваш номер телефона удален."
                : message == ManageMessageId.ChangeUserNameSuccess ? "Имя пользователя успешно изменено."
                : message == ManageMessageId.ChangeContactsSuccess ? "Ваши контакты успешно изменены."
                : message == ManageMessageId.ChangeEmailSuccess ? "Ваш E-mail успешно изменен."
                : message == ManageMessageId.ChangeAboutSuccess ? "Ваше примечание успешно изменено."
                : message == ManageMessageId.AdminSet ? "Роль Администратора установлена."
                : message == ManageMessageId.RelogSuccess ? "Переподключение успешно."
                : "";

            var userId = User.Identity.GetUserId<int>();
            var user = await UserManager.FindByIdAsync(userId);
            var userRolesIds = user.Roles.Select(r => r.RoleId).ToList();
            var model = new IndexViewModel
            {
                Id = user.Id,
                UserName = user.UserName,
                Email = user.Email,
                IsEmailHidden = user.IsEmailHidden,
                About = user.About,

                Roles = await db.Roles.Where(r => userRolesIds.Contains(r.Id)).Select(r => r.ViewName).ToListAsync(),

                Contacts = new UserContactsViewModel(user),

                HasPassword = HasPassword(),
                PhoneNumber = await UserManager.GetPhoneNumberAsync(userId),
                TwoFactor = await UserManager.GetTwoFactorEnabledAsync(userId),
                Logins = await UserManager.GetLoginsAsync(userId),
                BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync(User.Identity.GetUserId())
            };
            return View(model);
        }

        //
        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            ManageMessageId? message;
            var result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId<int>(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: user.IsPersistent, rememberBrowser: false);
                }
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("ManageLogins", new { Message = message });
        }

        //
        // GET: /Manage/AddPhoneNumber
        public ActionResult AddPhoneNumber()
        {
            return View();
        }

        //
        // POST: /Manage/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            // Создание и отправка маркера
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId<int>(), model.Number);
            if (UserManager.SmsService != null)
            {
                var message = new IdentityMessage
                {
                    Destination = model.Number,
                    Body = "Ваш код безопасности: " + code
                };
                await UserManager.SmsService.SendAsync(message);
            }
            return RedirectToAction("VerifyPhoneNumber", new { PhoneNumber = model.Number });
        }

        //
        // POST: /Manage/EnableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EnableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId<int>(), true);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: user.IsPersistent, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // POST: /Manage/DisableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DisableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId<int>(), false);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // GET: /Manage/VerifyPhoneNumber
        public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId<int>(), phoneNumber);
            // Отправка SMS через поставщик SMS для проверки номера телефона
            return phoneNumber == null ? View("Error") : View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        }

        //
        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePhoneNumberAsync(User.Identity.GetUserId<int>(), model.PhoneNumber, model.Code);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: user.IsPersistent, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.AddPhoneSuccess });
            }
            // Это сообщение означает наличие ошибки; повторное отображение формы
            ModelState.AddModelError("", "Не удалось проверить телефон");
            return View(model);
        }

        //
        // POST: /Manage/RemovePhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemovePhoneNumber()
        {
            var result = await UserManager.SetPhoneNumberAsync(User.Identity.GetUserId<int>(), null);
            if (!result.Succeeded)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: user.IsPersistent, rememberBrowser: false);
            }
            return RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        }

        //
        // GET: /Manage/ChangePassword
        public async Task<ActionResult> ChangePassword()
        {
            int userId = User.Identity.GetUserId<int>();
            if (await db.IsUserBannedAsync(userId))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            int userId = User.Identity.GetUserId<int>();
            if (await db.IsUserBannedAsync(userId))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(userId, model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(userId);
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: user.IsPersistent, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View(model);
        }

        //
        // GET: /Manage/SetPassword
        public ActionResult SetPassword()
        {
            return View();
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await UserManager.AddPasswordAsync(User.Identity.GetUserId<int>(), model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
                    if (user != null)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: user.IsPersistent, rememberBrowser: false);
                    }
                    return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                }
                AddErrors(result);
            }

            // Это сообщение означает наличие ошибки; повторное отображение формы
            return View(model);
        }

        //
        // GET: /Manage/ManageLogins
        public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.RemoveLoginSuccess ? "Внешнее имя входа удалено."
                : message == ManageMessageId.Error ? "Произошла ошибка."
                : "";
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
            if (user == null)
            {
                return View("Error");
            }
            var userLogins = await UserManager.GetLoginsAsync(User.Identity.GetUserId<int>());
            var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
            ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        //
        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Запрос перенаправления к внешнему поставщику входа для связывания имени входа текущего пользователя
            return new AccountController.ChallengeResult(provider, Url.Action("LinkLoginCallback", "Manage"), User.Identity.GetUserId());
        }

        //
        // GET: /Manage/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId<int>(), loginInfo.Login);
            return result.Succeeded ? RedirectToAction("ManageLogins") : RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }

                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Вспомогательные приложения
        // Используется для защиты от XSRF-атак при добавлении внешних имен входа
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId<int>());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber()
        {
            var user = UserManager.FindById(User.Identity.GetUserId<int>());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error,
            ChangeContactsSuccess,
            ChangeUserNameSuccess,
            ChangeEmailSuccess,
            ChangeAboutSuccess,
            AdminSet,
            RelogSuccess,
        }

        #endregion

        public async Task<ActionResult> ChangeEmail()
        {
            if (await db.IsUserBannedAsync(User.Identity.GetUserId<int>()))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
            return View(new ChangeEmailViewModel { OldEmail = user.Email, HideEmail = user.IsEmailHidden });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeEmail(ChangeEmailViewModel model)
        {
            int userId = User.Identity.GetUserId<int>();
            if (await db.IsUserBannedAsync(userId))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            var user = await UserManager.FindByIdAsync(userId);
            string email = user.Email;
            bool isEmailHidden = user.IsEmailHidden;

            if (ModelState.IsValid)
            {
                if (model.NewEmail != null)
                    user.Email = model.NewEmail;
                user.IsEmailHidden = model.HideEmail;

                user.LastAction = DateTime.Now;
                var result = await UserManager.UpdateAsync(user);
                if (result == IdentityResult.Success)
                    return RedirectToAction("Index", new { message = ManageMessageId.ChangeEmailSuccess });

                AddErrors(result);
            }

            return View(new ChangeEmailViewModel { OldEmail = email, HideEmail = isEmailHidden });
        }

        public async Task<ActionResult> ChangeUserName()
        {
            if (await db.IsUserBannedAsync(User.Identity.GetUserId<int>()))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeUserName(ChangeUserNameViewModel model)
        {
            int userId = User.Identity.GetUserId<int>();
            if (ModelState.IsValid && await db.IsUserNotBannedAsync(userId))
            {
                var user = await UserManager.FindByIdAsync(userId);
                user.UserName = model.NewUserName;

                user.LastAction = DateTime.Now;
                var result = await UserManager.UpdateAsync(user);
                if (result == IdentityResult.Success)
                {
                    await SignInManager.SignInAsync(user, isPersistent: user.IsPersistent, rememberBrowser: false);
                    return RedirectToAction("Index", new { message = ManageMessageId.ChangeUserNameSuccess });
                }

                AddErrors(result);
            }

            return View();
        }

        public async Task<ActionResult> ChangeContacts()
        {
            int userId = User.Identity.GetUserId<int>();
            if (await db.IsUserBannedAsync(userId))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            var user = await UserManager.FindByIdAsync(userId);
            var model = new UserContactsViewModel(user);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeContacts([Bind(Include = "EsoUserId,Discord,Telegram,Skype")] UserContactsViewModel model)
        {
            int userId = User.Identity.GetUserId<int>();
            if (ModelState.IsValid && await db.IsUserNotBannedAsync(userId))
            {
                var user = await UserManager.FindByIdAsync(userId);
                user.Discord = model.Discord;
                user.Skype = model.Skype;
                user.Telegram = model.Telegram;

                user.LastAction = DateTime.Now;
                var result = await UserManager.UpdateAsync(user);
                if (result == IdentityResult.Success)
                    return RedirectToAction("Index", new { message = ManageMessageId.ChangeContactsSuccess });

                AddErrors(result);
            }
            return View(model);
        }

        public async Task<ActionResult> ChangeAbout()
        {
            int userId = User.Identity.GetUserId<int>();
            if (await db.IsUserBannedAsync(userId))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            var user = await UserManager.FindByIdAsync(userId);
            return View(new ChangeAboutViewModel { About = user.About });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeAbout([Bind(Include = "About")] ChangeAboutViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
                user.About = model.About;

                user.LastAction = DateTime.Now;
                var result = await UserManager.UpdateAsync(user);
                if (result == IdentityResult.Success)
                    return RedirectToAction("Index", new { message = ManageMessageId.ChangeAboutSuccess });

                AddErrors(result);
            }
            return View(model);
        }

        public async Task<ActionResult> SetAdmin()
        {
            int adminId = await db.Roles.Where(r => r.Name == "Admin").Select(r => r.Id).SingleAsync();
            if (db.UserRoles.Where(ur => ur.RoleId == adminId).Any())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
            db.UserRoles.Add(new UserRole { UserId = user.Id, RoleId = adminId });
            await db.SaveChangesAsync();

            await SignInManager.SignInAsync(user, user.IsPersistent, false);

            return RedirectToAction("Index", new { message = ManageMessageId.AdminSet });
        }

        public async Task<ActionResult> Relog()
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
            user.LastAction = DateTime.Now;
            if (await UserManager.UpdateAsync(user) == IdentityResult.Success)
            {
                await SignInManager.SignInAsync(user, user.IsPersistent, false);
                return RedirectToAction("Index", new { message = ManageMessageId.RelogSuccess });
            }
            else
                return RedirectToAction("Index", new { message = ManageMessageId.Error });
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> ChangeUserPassword(int id)
        {
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return View(new ChangeUserPasswordViewModel { Id = id, UserName = user.UserName });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> ChangeUserPassword(ChangeUserPasswordViewModel model)
        {
            var user = await UserManager.FindByIdAsync(model.Id);
            if (user == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (ModelState.IsValid)
            {
                if (await AdminPasswordCorrect(model.AdminPassword))
                {
                    var result = await UserManager.RemovePasswordAsync(user.Id);
                    if (result == IdentityResult.Success)
                    {
                        result = await UserManager.AddPasswordAsync(model.Id, model.NewPassword);
                        if (result == IdentityResult.Success)
                            return RedirectToAction("Details", "Users", new { id = model.Id });
                    }
                    AddErrors(result);
                }
                else
                    ModelState.AddModelError("", "Неверный пароль администратора");
            }

            return View(new ChangeUserPasswordViewModel { Id = user.Id, UserName = user.UserName });
        }

        private async Task<bool> AdminPasswordCorrect(string password)
        {
            int id = await db.UserRoles.Where(ur => ur.RoleId == db.Roles.Where(r => r.Name == "Admin").Select(r => r.Id).FirstOrDefault()).Select(ur => ur.UserId).SingleAsync();
            var user = await UserManager.FindByIdAsync(id);
            var result = UserManager.PasswordHasher.VerifyHashedPassword(user.PasswordHash, password);
            return result == PasswordVerificationResult.Success;
        }
    }
}