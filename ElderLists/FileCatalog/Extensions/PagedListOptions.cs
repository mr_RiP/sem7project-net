﻿using FileCatalogDatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileCatalog.Extensions
{
    public class PagedListOptions
    {
        public PagedListOptions()
        {
            _page = 1;
            _pageSize = 10;
        }

        public PagedListOptions(string search, int? page, int pageSize = 10)
        {
            Search = search;
            Page = page ?? 1;
            PageSize = pageSize;
        }

        public string Search { get; set; }

        private int _page;
        public int Page
        {
            get
            {
                return _page;
            }

            set
            {
                _page = value < 1 ? 1 : value;
            }
        }

        private int _pageSize;
        public int PageSize
        {
            get
            {
                return _pageSize;
            }

            set
            {
                if (value < 1)
                    throw new ArgumentException();
                _pageSize = value;
            }
        }
    }
}