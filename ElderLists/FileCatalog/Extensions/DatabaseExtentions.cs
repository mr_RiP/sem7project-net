﻿using FileCatalog.Repositories;
using FileCatalogDatabase;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FileCatalog.Extensions
{
    public static class DatabaseExtentions
    {
        public static async Task<bool> IsUserInRoleAsync(this DatabaseContext db, int userId, string roleName)
        {
            var role = await db.Roles.FirstOrDefaultAsync(r => r.Name == roleName);
            return role == null ? false : await db.UserRoles.AnyAsync(ur => ur.UserId == userId && ur.RoleId == role.Id);
        }

        public static async Task<bool> IsUserNotBannedAsync(this DatabaseContext db, int userId)
        {
            return !(await IsUserInRoleAsync(db, userId, "Banned")); 
        }

        public static async Task<bool> IsUserBannedAsync(this DatabaseContext db, int userId)
        {
            return await IsUserInRoleAsync(db, userId, "Banned");
        }

        public static async Task UpdateUserLastActionAsync(this DatabaseContext db, int userId, DateTime time)
        {
            var user = await db.Users.FirstOrDefaultAsync(u => u.Id == userId);
            if (user != null)
            {
                user.LastAction = time;
                await db.SaveChangesAsync();
            }
        }

        public static async Task<string> GetUserNameAsync(this DatabaseContext db, int userId)
        {
            return await db.Users.Where(u => u.Id == userId).Select(u => u.UserName).FirstOrDefaultAsync();
        }
    }
}