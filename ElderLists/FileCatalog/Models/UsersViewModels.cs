﻿using FileCatalogDatabase.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FileCatalog.Models
{
    public class UserListItemViewModel
    {
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "Имя")]
        public string UserName { get; set; }

        [Display(Name = "Публикации")]
        public int Posts { get; set; }

        [Display(Name = "Правки")]
        public int Editions { get; set; }

        [Display(Name = "Дата последнего действия")]
        public DateTime LastAction { get; set; }
    }

    public class UserDetailsViewModel
    {
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }

        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Display(Name = "E-mail скрыт")]
        public bool IsEmailHidden { get; set; }

        [Display(Name = "Примечание")]
        [DataType(DataType.MultilineText)]
        [StringLength(1024)]
        public string About { get; set; }

        [Display(Name = "Дата регистрации")]
        public DateTime RegisterDate { get; set; }

        [Display(Name = "Дата последнего действия")]
        public DateTime LastAction { get; set; }

        [Display(Name = "Контакты")]
        public UserContactsViewModel Contacts { get; set; }

        [Display(Name = "Роли")]
        public UserRolesViewModel Roles { get; set; }

        [Display(Name = "Материалы")]
        public UserStatsViewModel Stats { get; set; }
    }

    public class UserStatsViewModel
    {
        [Display(Name = "Количество публикаций")]
        public int Posts { get; set; }

        [Display(Name = "Количество файлов")]
        public int Files { get; set; }

        [Display(Name = "Количество правок")]
        public int Editions { get; set; }
    }

    public class UserContactsViewModel
    {
        public UserContactsViewModel()
        {
        }

        public UserContactsViewModel(User user)
        {
            Discord = user.Discord;
            Skype = user.Skype;
            Telegram = user.Telegram;
        }

        [StringLength(32)]
        [Display(Name = "Telegram")]
        public string Telegram { get; set; }

        [StringLength(32)]
        [Display(Name = "Discord")]
        public string Discord { get; set; }

        [StringLength(32)]
        [Display(Name = "Skype")]
        public string Skype { get; set; }
    }

    public class UserRolesViewModel
    {
        [Display(Name = "Роли")]
        public IEnumerable<string> Roles { get; set; }

        [Display(Name = "В роли администратора")]
        public bool IsAdmin { get; set; }

        [Display(Name = "В роли модератора")]
        public bool IsModerator { get; set; }

        [Display(Name = "В роли пользователя")]
        public bool IsUser { get; set; }

        [Display(Name = "Забанен")]
        public bool IsBanned { get; set; }
    }
}