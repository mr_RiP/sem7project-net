﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FileCatalog.Models
{
    public class FileViewModelBase
    {
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "Имя")]
        public string Name { get; set; }
    }

    public class FileViewModel : FileViewModelBase
    {
        [Display(Name = "Скачано раз")]
        public int DownloadsCount { get; set; }

        [Display(Name = "Дата загрузки")]
        public DateTime UploadDate { get; set; }

        [Display(Name = "Дата последнего скачивания")]
        public DateTime? LastDownloadDate { get; set; }
    }

    public class FileEditViewModel : FileViewModel
    {
        [Display(Name = "Удалить")]
        public bool Delete { get; set; }
    }
}