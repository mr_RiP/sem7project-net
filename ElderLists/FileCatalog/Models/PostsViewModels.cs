﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FileCatalog.Models
{
    public class PostHeaderBaseViewModel
    {
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "Заголовок")]
        public string Header { get; set; }
    }

    public class PostHeaderViewModel : PostHeaderBaseViewModel
    {
        [Display(Name = "Скрыт")]
        public bool IsHidden { get; set; }

        [Display(Name = "Создано")]
        public DateTime CreationDate { get; set; }

        [Display(Name = "Отредактировано")]
        public DateTime? LastEditDate { get; set; }

        [Display(Name = "ID Автора")]
        public int AuthorId { get; set; }

        [Display(Name = "Автор")]
        public string Author { get; set; }

        [Display(Name = "ID Редактора")]
        public int? EditorId { get; set; }

        [Display(Name = "Редактор")]
        public string Editor { get; set; }
    }

    public class PostDetailsViewModel : PostHeaderViewModel
    {
        [Display(Name = "Текст")]
        public string Text { get; set; }

        [Display(Name = "Важно")]
        public bool IsImportant { get; set; }

        [Display(Name = "Исправления")]
        public IList<EditionViewModel> Editions { get; set; }

        [Display(Name = "Прикрепленные файлы")]
        public IList<FileViewModel> Files { get; set; }
    }

    public class PostsHeadersListsViewModel
    {
        [Display(Name = "Важные")]
        public IList<PostHeaderViewModel> Important { get; set; }

        [Display(Name = "Остальные")]
        public IPagedList<PostHeaderViewModel> Common { get; set; }
    }

    public class NewPostViewModel
    {
        [Required]
        [StringLength(128)]
        [Display(Name = "Заголовок")]
        public string Header { get; set; }

        [Required]
        [StringLength(2048)]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Текст")]
        public string Text { get; set; }

        [Display(Name = "Скрыть")]
        public bool IsHidden { get; set; }

        [Display(Name = "Важно")]
        public bool IsImportant { get; set; }
    }

    public class EditPostViewModel : NewPostViewModel
    {
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Required]
        [StringLength(512)]
        [Display(Name = "Замечания об исправлении")]
        public string Note { get; set; }

        [Display(Name = "Файлы")]
        public IEnumerable<FileEditViewModel> CurrentFiles { get; set; }
    }

    public class EditionViewModel
    {
        [Display(Name = "Дата")]
        public DateTime Date { get; set; }

        [Display(Name = "Исправление")]
        public string Note { get; set; }

        [Display(Name = "ID Редактора")]
        public int EditorId { get; set; }

        [Display(Name = "Отредактировал")]
        public string Editor { get; set; }
    }
}