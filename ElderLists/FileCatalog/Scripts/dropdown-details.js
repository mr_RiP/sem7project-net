﻿/* Информация о классе */
$(document).ready(function () {
    var selectedID = $('#class-dropdown').val();
    $.get('/Characters/ClassDetails/' + selectedID, function (data) {
        $('#class-details').html(data);
    });
});

$('#class-dropdown').change(function () {
    var selectedID = $(this).val();
    $.get('/Characters/ClassDetails/' + selectedID, function (data) {
        $('#class-details').html(data);
    });
});

/* Сокрытие поля долгожителя */
$(document).ready(function () {
    var raceId = $('#race-dropdown').val();
    $.ajax({
        url: '/Characters/IsRaceLongLiver',
        data: { raceId: raceId },
        success: function (data) {
            if (data) {
                $('#long-liver-input').show();
            } else {
                $('#long-liver-input').hide();
            }
        }
    });
});


$('#race-dropdown').change(function () {
    var raceId = $(this).val();
    $.ajax({
        url: '/Characters/IsRaceLongLiver',
        data: { raceId: raceId },
        success: function (data) {
            if (data) {
                $('#long-liver-input').show();
            } else {
                $('#long-liver-input').hide();
            }
        }
    });
});
    
/* Информация о расе */
$(document).ready(function () {
    var selectedID = $('#race-dropdown').val();
    $.get('/Characters/RaceDetails/' + selectedID, function (data) {
        $('#race-details').html(data);
    });
});

$('#race-dropdown').change(function () {
    var selectedID = $(this).val();
    $.get('/Characters/RaceDetails/' + selectedID, function (data) {
        $('#race-details').html(data);
    });
});

/* Информация о мировоззрении */
$(document).ready(function () {
    var selectedID = $('#alignment-dropdown').val();
    $.get('/Characters/AlignmentDetails/' + selectedID, function (data) {
        $('#alignment-details').html(data);
    });
});

$('#alignment-dropdown').change(function () {
    var selectedID = $(this).val();
    $.get('/Characters/AlignmentDetails/' + selectedID, function (data) {
        $('#alignment-details').html(data);
    });
});